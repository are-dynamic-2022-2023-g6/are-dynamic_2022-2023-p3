# ARE G6 - Modélisation du traffic global parisien

Par Anatole SAINERO, Thomas VEY, Aloïs MERELLE et Frantzley DURAND,
élèves de Sorbonne Université, en L1 portail Sciences Formelles.

## Description

Ce projet vise à répondre à la problématique suivante :
> *Comment illustrer les choix et les tendances des usagers pour leurs déplacements urbains en prenant pour objet d’étude la fluidité du trafic ?*

Cela passera par la modélisation et la simulation d'agents pouvant décider de leur moyen de transport quotidien, en parallèle de sous-modèles permettant d'associer le nombre d'usager à l'efficacité d'un certain moyen de transport.

## Site web

Le site web récapitulatif est disponible à <https://are-dynamic-2022-2023-g6.gitlab.io/are-dynamic_2022-2023-p3/>
