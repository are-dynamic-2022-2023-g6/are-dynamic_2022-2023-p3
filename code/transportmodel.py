import numpy as np


class TransportModel:

    def __init__(self, capacity, slowdown_threshold=0, confort=(0, 1), vitesse=(0, 1), retard_maximum=1):
        """"Capacity : nb maximum d'utilisateurs (en nb d'agents ou en nb d'utilisateurs), 
        slowdown_threshold : une limite (en nb d'utilisateurs) à partir de laquelle la vitesse ralentis,
        slowdown_rate : la "vitesse" à laquelle la vitesse ralentis."""

        self.capacity = capacity
        self.slowdown_threshold = slowdown_threshold
        self.confort_minimum, self.confort_maximum = confort
        self.vitesse_minimum, self.vitesse_maximum = vitesse
        self.retard_maximum = retard_maximum
        self.history = []

    def update_history(self, n_users=0):
        """Au DEBUT de chaque journée, utiliser cette fonction avec le nombre d'utilisateurs qui ont l'intention d'utiliser ce moyen de transport."""
        self.history.append(n_users)

    def get_last_history(self, nb_jours=1):
        """Renvoie l'historique des nb_jours derniers jours, sous forme de (numero_jour, vitesse_jour)"""
        res = []
        if len(self.history) <= 0:
            return [(0, self.vitesse_maximum)]
        for i in range(max(len(self.history)-nb_jours+1, 0), len(self.history)+1, 1):
            res.append((i, self.get_speed(i)))
        return res

    def clear_history(self):
        self.history.clear()

    def remove_history(self, n=0):
        """Enleve le jour n de l'historique, pas forcément utile."""
        if self.history:
            self.history.pop(n)

    def get_speed(self, n):
        """Renvoie la vitesse (entre 0 et 1) du jour n, pour l'instant linéairement (nul)"""
        n_users = 0
        if len(self.history) >= n:
            n_users = self.history[n-1]
        if n_users >= self.capacity:
            return self.vitesse_minimum
        elif n_users <= 0 or n_users <= self.slowdown_threshold:
            return self.vitesse_maximum
        
        elif n_users >= self.slowdown_threshold:
            nb_effectif = n_users - self.slowdown_threshold
            capacite_effective = self.capacity - self.slowdown_threshold
            pente = (self.vitesse_minimum - self.vitesse_maximum)/float(capacite_effective)
            test = self.vitesse_maximum + pente * nb_effectif
            return max(test, self.vitesse_minimum)
        else:
            return self.vitesse_maximum

    def get_last_confort(self):
        nb_users = self.get_last_history(1)[-1][1]
        if self.capacity <= 0:
            return 0
        return max(min(nb_users/self.capacity, self.confort_maximum), self.confort_minimum)

    def get_retard(self):
        return np.random.uniform(0, self.retard_maximum)
