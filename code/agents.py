import random
from transportmodel import TransportModel
import numpy as np
from graphics import Visualiser


class Agent:
    def __init__(self, preferences: dict[str, float]):
        self.preferences = preferences
        self.history = []  # à utiliser of course

    def choose(self, submodels: dict[str, TransportModel], parameters, force_prefered=False):

        if force_prefered:
            values = list(self.preferences.values())
            best_pref_today = np.random.choice(
                a=list(self.preferences.keys()), p=values)
            self.history.append(best_pref_today)
            return best_pref_today

        mode_hier = ""
        if len(self.history) >= 1:
            mode_hier = self.history[-1]

        last_speeds = {}  # les vitesses "hier" ou "dans la semaine"
        for k in submodels:
            if k == mode_hier:
                last_speeds[k] = submodels[k].get_last_history(1)[-1][1]
            else:
                speeds = submodels[k].get_last_history(
                    parameters["info sur n jours"])
                moyenne_speeds = np.mean(list(map(lambda t: t[1], speeds)))
                last_speeds[k] = moyenne_speeds

        delay = {}
        for k in submodels:
            delay[k] = submodels[k].get_retard()

        temps_de_trajet = {}
        distance_moyenne = 1
        for k in last_speeds:
            # delay est comme un pourcentage, 1 pour un trajet 2* plus long que prévu (à refaire c'est moche)
            temps_de_trajet[k] = (
                distance_moyenne/max(last_speeds[k], 0.0001)) + delay[k]
            # pour calculer le temps de trajet, l'agent fait une simplification : le temps de trajet de la veille (distance/vitesse)
            # le retard du jour est pour l'instant uniforme entre 0 et 1 pour tous les sous-modèles.

        vitesse_hier = 1
        if len(self.history) >= 1:
            # vitesse du transport pris hier -> veut-il le reprendre aujourd'hui ?
            vitesse_hier = submodels[mode_hier].get_last_history(1)[-1][1]

        confort = {}
        for k in submodels:
            confort[k] = submodels[k].get_last_confort()

        # doit être correspondre à l'historique et un peu d'aléatoire.
        preference_humaine = {}
        # les valeurs des préférences de l'agent
        values = list(self.preferences.values())
        # le truc préféré du jour (aléatoire), en fonction des preferences.
        best_pref_today = np.random.choice(
            a=list(self.preferences.keys()), p=values)

        for k in submodels:  # me demandez pas ce que ça fait je sais pas
            if k == best_pref_today:  # permet de lisser tout ça
                preference_humaine[k] = 1
            else:
                preference_humaine[k] = 0

        best_mode = None
        best_score = None

        fct1 = parameters["* self.pref"]
        fct2 = parameters["* tps trajet"]
        fct3 = parameters["* pref du jour"]
        fct4 = parameters["* confort"]
        for k in temps_de_trajet:
            score = fct1*self.preferences[k] + (
                fct2 / temps_de_trajet[k]) + fct3*preference_humaine[k] + fct4*confort[k]

            # score = a*self.preferences[k] + b*(1 / temps_de_trajet[k]) + c*caprice[k] + d*confort[k]

            if best_mode is None and submodels[k].capacity > 0.0:
                best_mode = k
                best_score = fct1*self.preferences[best_mode] + (fct2 /
                                                                 temps_de_trajet[best_mode]) + fct3*preference_humaine[best_mode] + fct4*confort[best_mode]
            else:
                if submodels[k].capacity > 0.0 and score > best_score:
                    best_mode = k
                    best_score = score
        if best_mode is None:
            print("Impossible de trouver un moyen de transport possible !")
            raise(Exception("All scores are None"))
        self.history.append(best_mode)
        return best_mode


def world_simulation(preferences: dict[str, float], models: dict[str, TransportModel], parameters, graphics: Visualiser, force_first_day = True):
    """Retourne un dictionnaire avec tout plein de data.
    Pour faire des moyennes sur plusieurs tests, augmenter nb_of_test"""

    mean_modes_history = {}
    speeds_history = {}
    confort_history = {}

    for k in models:
        mean_modes_history[k] = []
        speeds_history[k] = []
        confort_history[k] = []
    nb_of_test = parameters["nb simul"]

    if sum(preferences.values()) != 1:
        graphics.print_into_gui("Warning : preferences do not sum to 1.")
        return [mean_modes_history, speeds_history, confort_history]

    for t in graphics.long_iter_wrapper(range(nb_of_test)):

        liste_agents = []
        nb_agents = parameters["nb agents"]
        preferences_values = np.random.dirichlet(
            list(preferences.values()), size=nb_agents)
        # permet d'avoir en moyenne sur tous les agents les preferences voulues
        for k in models:
            models[k].clear_history()

        for i in range(nb_agents):
            self_preferences = {}
            s = 0
            for k in models:
                self_preferences[k] = preferences_values[i][s]
                s += 1
            if sum(self_preferences.values()) != 1:
                self_preferences[list(
                    models.keys())[-1]] = max(1 - sum(list(self_preferences.values())[:-1]), 0)
            liste_agents.append(Agent(self_preferences))

        test_modes_history = {}
        test_speed_history = {}
        test_confort_history = {}
        for k in models:
            test_modes_history[k] = []
            test_speed_history[k] = []
            test_confort_history[k] = []
        nb_jours = parameters["nb jours"]
        for j in range(nb_jours):
            retards = {}
            for k in models:
                retards[k] = models[k].get_retard()
                test_modes_history[k].append(0)

            for a in liste_agents:
                best_mode = a.choose(models, parameters,
                                     force_prefered=(j == 0 and force_first_day))
                test_modes_history[best_mode][j] += 1
            for k in models:
                models[k].update_history(test_modes_history[k][j])
                test_speed_history[k].append(models[k].get_speed(j))
                test_confort_history[k].append(models[k].get_last_confort())
        for k in mean_modes_history:
            if len(mean_modes_history[k]) < len(test_modes_history):
                mean_modes_history[k] = test_modes_history[k].copy()
                speeds_history[k] = test_speed_history[k].copy()
                confort_history[k] = test_confort_history[k].copy()
            else:
                mean_modes_history[k] = np.average(
                    [mean_modes_history[k], test_modes_history[k]], weights=[t, 1], axis=0)
                speeds_history[k] = np.average(
                    [speeds_history[k], test_speed_history[k]], weights=[t, 1], axis=0)
                confort_history[k] = np.average(
                    [confort_history[k], test_confort_history[k]], weights=[t, 1], axis=0)

    return mean_modes_history, speeds_history, confort_history
