import tkinter as tk
from tkinter import filedialog, messagebox, ttk
from ttkthemes import ThemedTk
from ttkwidgets.frames import ScrolledFrame
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import matplotlib.pyplot as plt
import numpy as np
import os
import threading
from datetime import datetime
import json


class Visualiser(ThemedTk):
    """A Tk window that can be used with the simulation program to visualize differents plots and modify parameters."""

    def __init__(self, launch_functions, parameters):
        """Creates the window and all the widgets within it."""
        ThemedTk.__init__(self, theme="clam")
        self.title("Data Visualiser")
        self.geometry("800x700+500+250")

        def on_closing():
            if threading.active_count() > 1:
                answer = messagebox.askokcancel(
                    "EXITING", "A calculation is running, do you want to stop it and quit ?", icon=messagebox.WARNING)
                if answer:
                    self.destroy()
            else:
                self.destroy()
        self.protocol("WM_DELETE_WINDOW", on_closing)

        frame = ttk.Frame(self)
        self.fig = Figure()
        ax = self.fig.add_subplot(111)
        self.canvas = FigureCanvasTkAgg(self.fig, master=self)
        plot_toolbar = NavigationToolbar2Tk(
            self.canvas, self, pack_toolbar=False)
        plot_toolbar.update()
        self.canvas.draw_idle()
        self.down_frame = ttk.Frame(self, padding=(5, 5))

        self.progressbar = ttk.Progressbar(
            self.down_frame, orient="horizontal", mode="determinate")
        self.progressbar.pack(side="bottom", fill="x")
        sep = ttk.Separator(self.down_frame, orient="horizontal")
        sep.pack(side="bottom", fill="x", pady=5)

        self.launch_frame = ttk.Frame(self.down_frame, padding=(
            5, 5), relief="solid", borderwidth=0.5)
        self.launch_frame.pack(side="left", fill="both", expand=True)

        options_frame = ttk.Frame(self.down_frame, padding=(
            5, 5), relief="solid", borderwidth=0.5)
        options_frame.pack(side="left", fill="both", padx=2, expand=True)

        # stats_frame = ttk.Frame(self.down_frame, padding=(
        #     5, 5), relief="solid", borderwidth=0.5)
        # stats_frame.pack(side="right", fill="both", expand=True)

        # La liste de TOUTES les variables modifiables par l'interface graphique, sous forme de tk.Var() ici mais sous forme "normale" après Visualiser.get_parameters().
        self.variables = {}
        for (k, v) in parameters.items():
            if isinstance(v, bool):
                self.variables[k] = tk.BooleanVar(value=v)
            elif isinstance(v, float):
                self.variables[k] = tk.DoubleVar(value=v)
            elif isinstance(v, int):
                self.variables[k] = tk.IntVar(value=v)
            else:
                # TODO : si besoin d'autres types que Bool (valeurs etc), ajouter les autres types.
                messagebox.showwarning(
                    "PARAMETRES", "Ce type de paramètres n'est pas pris en charge.\n"+str(k))
        if os.path.isfile("options.json"):
            yes = messagebox.askyesno(
                "PARAMETERS", "Charger les paramètres de options.json ?")
            if yes:
                self.load_parameters("options.json")

        # Pour ajouter des boutons ou autre widgets, c'est ici
        # en suivant le modèle suivant :
        # button = ttk.Button(<là où ça va>, text = "text", command = dosomething)
        # button.pack()

        for (k, v) in launch_functions.items():
            b = ttk.Button(self.launch_frame, text=k,
                           command=lambda v=v: smart_calculation(v))
            b.pack(side="top")

        parameters_frame = ScrolledFrame(
            options_frame, compound=tk.RIGHT, canvasheight=100, canvasborder=3)
        for k in self.variables:
            if isinstance(parameters[k], bool):
                checkbox = ttk.Checkbutton(
                    parameters_frame.interior, text=k, variable=self.variables[k], onvalue=True, offvalue=False)
                checkbox.pack(side="top")
            elif isinstance(parameters[k], float):
                f = ttk.Frame(parameters_frame.interior)
                l = ttk.Label(f, text=k)
                l.pack(side="left")
                slider = ttk.Spinbox(f, from_=0.0, increment=0.05, to=1.0,
                                     textvariable=self.variables[k], justify="right", width=4)
                slider.pack(side="right", fill="x")
                f.pack(side="top", fill="both", expand=True)
            elif isinstance(parameters[k], int):
                f = ttk.Frame(parameters_frame.interior)
                l = ttk.Label(f, text=k)
                l.pack(side="left")
                slider = ttk.Spinbox(f, from_=0, increment=1, to=9999,
                                     textvariable=self.variables[k], justify="right", width=4)
                slider.pack(side="right", fill="x")
                f.pack(side="top", fill="both", expand=True)
            else:
                # TODO: ajouter des sliders ou autres moyen de choisir des valeurs.
                pass
        parameters_frame.pack(side="top", fill="both", expand=True)
        save_parameters_button = ttk.Button(
            options_frame, text="Save options", command=self.save_parameters)
        save_parameters_button.pack(side="bottom")
        load_parameters_button = ttk.Button(
            options_frame, text="Load options", command=self.load_parameters)
        load_parameters_button.pack(side="bottom")
        sep = ttk.Separator(options_frame)
        sep.pack(side="bottom", fill="x", pady=2)

        # self.stats_label = ttk.Label(stats_frame, justify="left")
        # self.stats_label.bind("<Configure>", lambda e: self.stats_label.config(
        #     wraplength=self.stats_label.winfo_width()))
        # self.stats_label.pack(side="left", fill="both", expand=True)

        # pas touche au reste thanks !
        self.down_frame.pack(side="bottom", expand=1, fill="both")
        plot_toolbar.pack(side="top", fill="x")
        self.canvas.get_tk_widget().pack(side="top", fill="both", expand=1)
        frame.pack()

        # self.mainloop()

    def plot(self, datax=None, multiple_datay=None, legend=None, color=None):
        """Draws one or multiple plots (with same x axis) on the principal canvas.
        Peut recevoir une ou plusieurs listes en y data et est normalement plutôt robuste face aux erreurs.
        Attention de bien fournir des listes de même longueur en x et y -> "ValueError : x and y have different shapes" """
        if threading.current_thread() == threading.main_thread() and threading.active_count() >= 2:
            for t in threading.enumerate():
                if t.name == "Calculation":
                    messagebox.showerror(
                        "PLOT", "Un lourd calcul est en cours, vous ne pouvez pas afficher autre chose pendant ce temps !")
                    return
        if multiple_datay is None:
            messagebox.showerror(
                "PLOT", "Plot function called without datay. Aborted.")
            return

        self.fig.clear()
        ax = self.fig.add_subplot(111)
        self.fig.subplots_adjust(left=0.1, bottom=0.095, right=0.96, top=0.93)

        # si datay est une liste de listes
        if all((isinstance(x, (list, np.ndarray, tuple))) for x in multiple_datay):
            x_range = datax
            if datax is None:
                x_range = np.arange(len(multiple_datay[0]))
            for i in range(len(multiple_datay)):
                c = color
                if isinstance(color, (list, tuple, np.ndarray)):
                    if i < len(color):
                        c = color[i]
                    else:
                        c = None
                _ = ax.plot(x_range, multiple_datay[i], color=c)

        # si datay contient des listes et des valeurs simples
        elif any((isinstance(x, (list, np.ndarray, tuple))) for x in multiple_datay):
            messagebox.showerror("Erreur", "Valeurs invalides pour plot")

        else:  # si datay ne contient que des valeurs simples <=> datay est la liste des données elle-même
            c = color
            if isinstance(color, (list, tuple, np.ndarray)):
                c = color[0]
            x_range = datax
            if datax is None:
                x_range = np.arange(len(multiple_datay))
            _ = ax.plot(x_range, multiple_datay, color=color)

        if legend is not None:
            if isinstance(legend, str):  # si un seul string a été donné dans l'option legend
                ax.legend([legend], loc="best")
            else:  # si une liste de string a été donnée
                ax.legend(legend, loc="best")

        self.canvas.draw_idle()

    def print_into_gui(self, text):
        """Envoie un message d'information."""
        messagebox.showinfo("INFO", text)

    def update_progressbar(self, value=0, force=False):
        """Updates the Progressbar to the given value, between 0 and 100
        A utiliser pour afficher l'avancée des longues simulations, mais à ne pas utiliser massivement car ralentis les calculs."""
        if force:
            self.progressbar["value"] = 100
            self.progressbar.update()
            self.progressbar.update_idletasks()
            return
        if abs(self.progressbar["value"] - value) >= 1:
            self.progressbar["value"] = min(100, value)
            if value > 100:
                messagebox.showerror(
                    "Progressbar", "La valeur donnée à la barre de chargement est trop élevée !")

    def get_parameters(self):
        """Renvoie un dictionnaire contenant tous les paramètres"""
        res = {}
        for k, v in self.variables.items():
            res[k] = v.get()
        return res

    def load_parameters(self, filename="", replace_values=True):
        """Lis les paramètres dans un fichier donné ou choisi."""
        if len(filename) > 0:
            if not os.path.isfile(filename):
                messagebox.showerror(
                    "FILE", "Le fichier donné n'existe pas. Lecture des paramètres abandonnée.")
                return
        else:
            data = [("json", "*.json"), ("all files", "*.*")]
            filename = filedialog.askopenfilename(
                filetypes=data, defaultextension=".json", initialdir=".")
            if not os.path.isfile(filename):
                messagebox.showerror(
                    "FILE", "Le fichier donné n'existe pas. Lecture des paramètres abandonnée.")
                return
        try:
            with open(filename, "r") as f:
                obj = json.load(f)["parameters"]
                if replace_values:
                    for k in self.variables:
                        if k in obj:
                            self.variables[k].set(obj[k])
                messagebox.showinfo("JSON", "Lecture des paramètres réussie !")
                return obj
        except Exception as e:
            messagebox.showerror(
                "JSON", "Erreur lors de la lecture du fichier. Vérifiez qu'il correspond à un fichier paramètres.")
            print(e)

    def save_parameters(self):
        """Sauvegarde les paramètres dans un fichier json choisi"""
        data = [("json", "*.json"), ("all files", "*.*")]
        try:
            with filedialog.asksaveasfile(
                    mode="w", filetypes=data, defaultextension=".json", initialdir=".", initialfile="options.json") as f:
                txttime = datetime(2000, 1, 1).now().isoformat(
                    sep=' ', timespec="seconds")
                tosave = json.dump(
                    {"time": txttime, "parameters": self.get_parameters()}, f, indent=4)
        except Exception as e:
            print("Erreur :", e)
            messagebox.showerror(
                "FILE", "Une erreur a eu lieu lors de l'enregistrement du fichier.")
            return
        else:
            messagebox.showinfo("FILE", "Le fichier a bien été enregistré !")

    def long_iter_wrapper(self, iter):
        """Wraps around an iterator to make the Progressbar update and a lot more quality of life stuff.
        A utiliser autour de l'iteration principale
        Utile pour les longs (pas instantanés) calculs principalement."""

        if not (hasattr(iter, '__iter__') or hasattr(iter, '__getitem__')):
            messagebox.showerror(
                "ITER", "The long iteration wrapper only works with range objects.")
            return

        for w in self.launch_frame.winfo_children():
            w.configure(state="disabled")

        self.fig.clear()
        self.fig.text(0.5, 0.5, "Calcul en cours...", color="red", fontsize=12,
                      horizontalalignment="center", verticalalignment="center")
        self.fig.text(0.5, 0.4, "Par mesure de sécurité, impossible de lancer d'autres calculs pendant ce temps.",
                      color="red", fontsize=8, horizontalalignment="center")
        self.fig.text(0.5, 0.3, "Pour l'arrêter prématurément (boucle infinie par exemple), fermez la fenêtre.",
                      color="red", fontsize=9, horizontalalignment="center")
        self.canvas.draw()
        self.update_progressbar(0, force=True)

        steps = iter.stop - iter.start
        count = 0

        for i in iter:
            count += 1
            self.update_progressbar(count*100/steps)
            yield i

        self.update_progressbar(100, force=True)
        for w in self.launch_frame.winfo_children():
            w.configure(state="normal")


def smart_calculation(function):
    """Launch the calculation in a thread so that the window stays responsive during it.
    A utiliser de préférence pour les longs calculs !"""
    threading.Thread(target=function, daemon=True, name="Calculation").start()


def base_stats(datay, graphics: Visualiser):
    moyenne = np.mean(datay)
    maxi = np.max(datay)
    mini = np.min(datay)
    ecart = np.std(datay)
    texte = f"Moyenne : {moyenne:10.4f}\nMaximum : {maxi:10.4f}\nMinimum : {mini:10.4f}\nEcart-type : {ecart:10.4f}"
    graphics.print_into_gui(texte)

# v = Visualiser()
