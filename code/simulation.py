from graphics import Visualiser, smart_calculation
from transportmodel import TransportModel
from agents import world_simulation
import numpy as np


def test(g, greve_metro=False, jo=False, tele=False, no_car=False, more_teletravail = False):
    # les paramètres du json, présents sur l'interface graphique
    parameters = g.get_parameters()

    bus = TransportModel(parameters["taille bus"], vitesse=(
        0.2, 0.3), slowdown_threshold=28, confort=(0.4, 0.8), retard_maximum=0.7)

    metro = TransportModel(
        parameters["taille metro"], vitesse=(0.3, 0.56), slowdown_threshold=60, confort=(0.3, 1), retard_maximum=0.8)

    voiture = TransportModel(parameters["taille voiture"], vitesse=(
        0.02, 0.4), slowdown_threshold=25, confort=(0.8, 1), retard_maximum=0.5)

    velo = TransportModel(parameters["taille velo"], vitesse=(
        0.1, 0.35), slowdown_threshold=20, confort=(0.4, 0.8), retard_maximum=0.2)

    teletravail = TransportModel(parameters["taille teletravail"], 10000000000, confort=(
        1, 1), vitesse=(0, parameters["vitesse teletravail"]), retard_maximum=0)

    if no_car:
        voiture.capacity = 1

    if tele:
        teletravail.capacity = 0
        teletravail.confort_maximum = 0
        teletravail.confort_minimum = 0

    if more_teletravail:
        teletravail.capacity = teletravail.capacity*4
        teletravail.vitesse_maximum = 1

    if greve_metro:
        metro.capacity = 0
        bus.capacity *= 0.5

        velo.capacity += 10
        voiture.slowdown_threshold -= 10

    if jo:
        metro.capacity += 20
        voiture.capacity += 100
        velo.capacity += 300

        metro.confort_maximum -= 0.6
        voiture.confort_maximum -= 0.5
        bus.confort_maximum -= 0.6
        velo.confort_maximum += 0.2

        metro.slowdown_threshold += 30
        voiture.slowdown_threshold -= 10
        velo.slowdown_threshold += 1000

    models = {"metro": metro, "voiture": voiture,
              "bus": bus, "velo": velo, "teletravail": teletravail}

    new_models = {}
    for k in models:
        if parameters[k] != 0:
            new_models[k] = models[k]
    models = new_models  # pour ne pas prendre en compte les modèles avec une capacité vide

    preferences = {}
    for k in models:
        preferences[k] = parameters[k]

    modes, speeds, confort = world_simulation(
        preferences, models, parameters, g, force_first_day=greve_metro)

    # tout ce qui est en-dessous c'est de l'affichage
    if "teletravail" in speeds:
        speeds.pop("teletravail")
    figure = g.fig
    figure.clear()
    figure.tight_layout(pad=10)
    axs = figure.subplots(3, 1, sharex=True)
    figure.subplots_adjust(left=0.05, bottom=0.05,
                           right=0.95, top=0.93, hspace=0.4)
    datax = np.arange(len(list(modes.values())[0]))
    axs[0].set_ylim(-0.01, parameters["nb agents"])
    axs[1].set_ylim(-0.01, 1.01)
    axs[2].set_ylim(-0.01, 1.01)
    axs[0].set_title("Nb d'agents")
    axs[1].set_title("Vitesse")
    axs[2].set_title("Confort")
    for v in list(modes.values()):
        axs[0].plot(datax, v)
    axs[0].legend(modes.keys(), loc="best")
    for v in list(speeds.values()):
        axs[1].plot(datax, v)
    for v in list(confort.values()):
        axs[2].plot(datax, v)
    g.canvas.draw_idle()
    # g.plot(multiple_datay=list(modes.values()), legend=modes.keys())


param = {
    "metro": 0.50, "voiture": 0.20, "teletravail": 0.1, "velo": 0.05, "bus": 0.15,
    "taille metro": 75, "taille voiture": 50, "taille teletravail": 30, "taille bus": 30, "taille velo": 35, "vitesse teletravail": 0.1,
    "nb jours": 10, "nb agents": 110, "nb simul": 20,
    "* self.pref": 0.7, "* tps trajet": 1, "* pref du jour": 0.2, "* confort": 0.8, "info sur n jours": 7}
# vitesse teletravail peut être relié à l'efficacité du télétravail face aux autres modes (?)
g = Visualiser({"Simulation normale - aucune gêne": (lambda: test(g)),
               "Grève des métros dès le jour 1": (lambda: test(g, True)),  "Experience JO": (lambda: test(g, False, True)), "Pas de teletravail": (lambda: test(g, False, False, True)), "Pas de voiture": (lambda: test(g, False, False, False, True))}, param)


g.mainloop()
